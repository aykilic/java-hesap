package com.proje.hesap.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link User} entity
 */
@Data
public class UserDto implements Serializable {
    private final Date createdAt;
    private final String createdBy;
    private final Date updatedAt;
    private final String updatedBy;
    private final Long id;
    private final String Name;
}