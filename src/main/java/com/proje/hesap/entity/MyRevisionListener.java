package com.proje.hesap.entity;

import org.hibernate.envers.RevisionListener;

public class MyRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {
        MyRevision rev = (MyRevision) revisionEntity;
//        rev.setUserName(getUserName());
        rev.setUserName("ibo");
    }
}
