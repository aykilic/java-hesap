package com.proje.hesap.entity;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Entity;

@Entity
@RevisionEntity(MyRevisionListener.class)
public class MyRevision extends DefaultRevisionEntity {

    private String userName;

    public String getUserName() {
//        return userName;
        return "ibo";
    }

    public void setUserName(String userName) {
//        this.userName = userName;
        this.userName = "ibo";
    }
}
