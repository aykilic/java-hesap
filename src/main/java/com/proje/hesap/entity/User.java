package com.proje.hesap.entity;

import lombok.Data;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;


@Audited
@Data
@Entity
@Table(name="Users")
public class User extends BaseEntity {
    @Id
    @SequenceGenerator(name="users_seq_gen",sequenceName = "users_gen",initialValue = 100,allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE  )
    @Column(name="ID")
    private Long id;
    @Column(length=100)
    private String Name;

}
