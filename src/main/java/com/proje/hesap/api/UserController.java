package com.proje.hesap.api;


import com.proje.hesap.dto.UserDto;
import com.proje.hesap.entity.User;
import com.proje.hesap.service.UserService;
import com.proje.hesap.util.CustomPage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/user")
public class UserController {
     private final UserService userService;
     public UserController(UserService userService) {
          this.userService = userService;
     }
     @PostMapping("/createUser")
     public ResponseEntity<UserDto> createUser(@RequestBody UserDto user){
          UserDto resultUser=userService.createUser(user);
          return ResponseEntity.ok(resultUser);
     }
     @GetMapping("/getAllUsers")
     public ResponseEntity<List<UserDto>> GetAllUsers(){
          List<UserDto> resultUser=userService.getAllUsers();
          return ResponseEntity.ok(resultUser);
     }

     @GetMapping("/getById/{id}")
     public ResponseEntity<UserDto> getUser(@PathVariable("id") Long Id){
          UserDto resultUser=userService.getUser(Id);
          return ResponseEntity.ok(resultUser);


     }
     @PutMapping("/editUser/{id}")
     public ResponseEntity<UserDto> updateUser(@PathVariable("id") Long Id,@RequestBody UserDto user){

          UserDto resultUser=userService.updateUser(Id,user);
          return ResponseEntity.ok(resultUser);

     }
     @DeleteMapping("/deleteUser/{id}")
     public ResponseEntity<Boolean> deleteUser(@PathVariable("id") Long Id){
          Boolean status=userService.deleteUser(Id);
          return ResponseEntity.ok(status);
     }
     @GetMapping("/pagination")
//     public List<AayYakin> findAllAayYakinByQueryFilterDto(AayYakinQueryFilterDto queryFilterDto, int startIndex, int pageSize) {
//          return getDao().findAllByQueryFilterDto(queryFilterDto, startIndex, pageSize);
//     }
     public ResponseEntity<Page<User>> pagination(@RequestParam int currentPage, @RequestParam int pageSize) {
          return ResponseEntity.ok(userService.pagination(currentPage, pageSize));
     }
     @GetMapping("/pagination/v1")
     public ResponseEntity<Page<User>> pagination(Pageable pageable) {
          return ResponseEntity.ok(userService.pagination(pageable));
     }
     @GetMapping("/pagination/v2")
     public ResponseEntity<Slice<User>> slice(Pageable pageable) {
          return ResponseEntity.ok(userService.slice(pageable));
     }
     @GetMapping("/pagination/v3")
     public ResponseEntity<CustomPage<UserDto>> customPagination(Pageable pageable) {
          return ResponseEntity.ok(userService.customPagination(pageable));
     }

}
